Vanilla files will be stored in CDBV and PAKV.
Put the files you edited into CDB and PAK. You'll only work on the files you're interested in, instead of having to scroll through all the jsons.
Refer to the [Corrupted](https://gitlab.com/alaah/dead-cells-corrupted-mode) files for an example.

# Howto
`setup.bat` should guide you.
Alternatively, edit `config.bat` in a text editor to your needs, and **make sure the Dead Cells path is correct**.
Run `extract-vanilla.bat` to get a fresh setup.
Put the files to add/override into PAK and CDB. Leave PAKV and CDBV be, and use them for reference read-only.

# Files
* compile-all – exec compile-cdb and compile-pak
* compile-cdb – collapse the CDB dir into PAK/data.cdb
* compile-pak – build the mod and optionally launch dead cells
* config – variables; edit to your needs
* extract-vanilla – extracts the full res.pak

# License
See LICENSE for information