@echo off
move /Y bin\* . > NUL

echo Welcome to barebones

if exist "C:\Program Files (x86)\Steam\steamapps\common\Dead Cells\res.pak" (choice /M "Is the path C:\Program Files (x86)\Steam\steamapps\common\Dead Cells correct?")

if %errorlevel% equ 1 (
set dcpath=C:\Program Files ^(x86^)\Steam\steamapps\common\Dead Cells& goto :destructor
)

goto :customdir

:customdir
set /p dcpath=Paste the full path of your Dead Cells directory: & goto :verifydir

:verifydir
if exist "%dcpath%\res.pak" (goto :destructor) else (echo Invalid directory. & goto :customdir)

:destructor
@echo set dcpath=%dcpath%>>config.bat

call extract-vanilla.bat
echo.
echo How to use:
echo 1. Install the Dummy Mod from the workshop.
echo 2. Copy the files you want to edit from CDBV/PAKV ^(except for data.cdb^) to CDB/PAK, retaining all the subdirectories.
echo 3. Run compile-all and launch Dummy Mod.
echo 4. Run extract-vanilla each time there's a new Dead Cells update.
echo.
echo This file will now be autodestructed.
pause
pause
del setup.bat & exit