@echo off
call config.bat
rd /s /q CDBV > NUL
rd /s /q PAKV > NUL
mkdir PAK > NUL
mkdir CDB > NUL
mkdir PAKV > NUL
mkdir CDBV > NUL
"%dcpath%\ModTools\PAKTool.exe" -Expand -OutDir "%~dp0PAKV" -RefPak "%dcpath%\res.pak"
"%dcpath%\ModTools\CDBTool.exe" -Expand -OutDir "%~dp0CDBV" -RefCDB "%~dp0PAKV\data.cdb"
if %pause%==1 (pause)