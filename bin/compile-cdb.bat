@echo off
call config.bat
rd /s /q "%~dp0CDB0" > NUL
mkdir CDB0 > NUL
xcopy /s /q /y "%~dp0CDBV" "%~dp0CDB0" > NUL
xcopy /s /q /y "%~dp0CDB"  "%~dp0CDB0" > NUL
"%dcpath%\ModTools\CDBTool.exe" -Collapse -InDir "%~dp0CDB0" -OutCDB "%~dp0PAK\data.cdb"
rd /s /q "%~dp0CDB0"
if %pause%==1 (pause)